#!/usr/bin/env python
# PAP server
import asyncio
import websockets
import hashlib
import rsa
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from base64 import b64encode


async def serve(websocket, path):
    bob_pubkey = rsa.PublicKey(7036704199962751985482282860702885843845383370140838911770459862559881567841367494740566762619052237015327330846675176840497314020470869501804809635122843, 65537)
    bob_privkey = rsa.PrivateKey(7036704199962751985482282860702885843845383370140838911770459862559881567841367494740566762619052237015327330846675176840497314020470869501804809635122843, 65537, 5159984900770708682397844724654153957055120527369706218195290600346411768065673079190329175956077431621490540256740160708656721170018567138708006686729553, 7149702909485353015132443511111019127255910442306190134608794595276924762950413261, 984195327980315358802522329505160258435359334405235997172786604714003463)
    my_pubkey = f"This is my public key: {bob_pubkey.n}, {bob_pubkey.e}".encode()

    print(f"[0] Sending Bob's public key to Alice: {bob_pubkey}")
    await websocket.send(my_pubkey)

    alice_message_with_pubkey = await websocket.recv()
    print(f"[0] Got Alices's Public Key: {alice_message_with_pubkey}")
    alice_n,alice_e = alice_message_with_pubkey.decode().split(": ")[1].split(",")
    alice_pubkey = rsa.PublicKey(int(alice_n), int(alice_e))

    encrypted_challenge_R2 = await websocket.recv()
    challenge_R2 = rsa.decrypt(encrypted_challenge_R2, bob_privkey)
    print(f"[2] Got new challenge_R2 '{challenge_R2}' from Alice encrypted with my pubkey")

    challenge_R1 = 'This is challenge_R1 from Bob'.encode()
    encrypted_challenge_R1 = rsa.encrypt(challenge_R1, alice_pubkey)

    print(f"[3] I send clear challenge_R2 {challenge_R2} and challenge_R1 encrypted with Alice's pubkey '{alice_pubkey}' -> {encrypted_challenge_R1}'")
    await websocket.send(challenge_R2)
    await websocket.send(encrypted_challenge_R1)

    challenge_R1_from_Alice = await websocket.recv()
    
    if challenge_R1_from_Alice == challenge_R1:
        print("Mutual Authentication succeed!")
    else:
        print("Something is wrong! Someone's trying to trick you!")
        

print("Server started!")
start_server = websockets.serve(serve, "localhost", 1234)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
