#!/usr/bin/env python
# PAP client
import asyncio
import websockets
import hashlib
import rsa
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from base64 import b64decode


async def try_auth(uri):
    async with websockets.connect(uri) as websocket:
        alice_pubkey = rsa.PublicKey(7342261860758121841300379424420153791948790006935035671405621083565344206782727216487176973695096106538241289597689501815720955443799441378924284062687623, 65537)
        alice_privkey = rsa.PrivateKey(7342261860758121841300379424420153791948790006935035671405621083565344206782727216487176973695096106538241289597689501815720955443799441378924284062687623, 65537, 3049183530894819753035269035727044658062929606462774705412923829766360577359700493269661742777882935022613144598705081943797755604669405672256102992049441, 4320196765898586603286965876790324330374116341204029149498625932181825360448034999, 1699520243780136185122241915774062378550822734810837658826277443591507377)
        my_pubkey = f"This is my public key: {alice_pubkey.n}, {alice_pubkey.e}".encode()
        challenge_R2 = 'This is challenge_R2 from Alice'.encode()

        bob_message_with_pubkey = await websocket.recv()
        print(f"[0] Got Bob's Public Key: {bob_message_with_pubkey}")
        bob_n,bob_e = bob_message_with_pubkey.decode().split(": ")[1].split(",")

        print(f"[0] Sending my public key to bob: {my_pubkey}")
        await websocket.send(my_pubkey)

        bob_pubkey = rsa.PublicKey(int(bob_n), int(bob_e))
        
        encrypted_challenge_R2 = rsa.encrypt(challenge_R2, bob_pubkey)

        print(f"[1] I send challenge_R2 {challenge_R2} encrypted with Bob's pubkey '{bob_pubkey}' -> {encrypted_challenge_R2}'")
        await websocket.send(encrypted_challenge_R2)

        challenge_R2_from_Bob = await websocket.recv()
        encrypted_challenge_R1_from_Bob = await websocket.recv()

        challenge_R1_from_Bob = rsa.decrypt(encrypted_challenge_R1_from_Bob, alice_privkey) 
        print(f"[4] I got clear text challenge R2 from Bob '{challenge_R2_from_Bob}' and challenge_R1 '{challenge_R1_from_Bob}'")

        print(f"[5] I send clear challenge_R1 {challenge_R1_from_Bob} to Bob") 
        await websocket.send(challenge_R1_from_Bob)


asyncio.get_event_loop().run_until_complete(
    try_auth('ws://localhost:1234')
)
