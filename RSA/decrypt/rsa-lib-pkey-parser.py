from rsa import PublicKey

with open('pubkeys.txt', 'r') as f:
    for count,line in enumerate(f.readlines()):
        c,pubkey = line.strip("\n").split("_")
        pubkey = eval(pubkey)
        print(pubkey.n, pubkey.e)

        pubkey_pkcs1 = pubkey.save_pkcs1().decode() # insecure code
        filename = f"{count}.pub"
        f = open(filename, "w")
        f.writelines(pubkey_pkcs1)
        f.write(f"{pubkey.n};{pubkey.e}"+"\n")
        print(f"Created file: {filename}")
        f.close()
