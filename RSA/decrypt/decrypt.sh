
pubkeys=$(ls pubkeys/*.pub)

for filename in $pubkeys
do
    echo $filename
    n=$(grep ";" $filename | cut -d ";" -f1)
    e=$(grep ";" $filename | cut -d ";" -f2)
    python3 RsaCtfTool.py -n $n -e $e --dumpkey --private 2>&1| grep "END" -A5 | tail -n5 | strings | cut -c 8-8000 | tee $filename.priv
done
